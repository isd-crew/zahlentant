print('this will be the final task')

n = 7 # is this a prime?
t = 2 # try dividing n by t


x = 2 # divisor
z = 0
l = 0 # period length of x

while n < 1000:
  while t < n and n % t != 0:
    t = t + 1

  if t == n: # this is a prime
    while z != 10:
      if z == 0:
        z = 10
      while z > n:
        z = z - n
      z = z * 10
      l = l + 1
    z = 0
    print('p:\t', n, '\tl: ', l, '\t', (n-1)/l)
    l = 0

  elif n % 2 != 0 and n % 5 != 0: # this is not a prime
    while z != 10:
      if z == 0:
        z = 10
      while z > n:
        z = z - n
      z = z * 10
      l = l + 1
    z = 0
    print('np:\t', n, '\tl: ', l)
    l = 0

  t = 2
  n = n + 1


